﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsolaVisual
{
    class Program
    {
         static void Main(string[] args)
        {
            bool repetir = true;
            Console.WriteLine("Introduzca un número: ");
            String captura = Console.ReadLine();
            int numero = int.Parse(captura);
            while (repetir)
            {
                Console.WriteLine("1-Generar "+numero+" numeros aleatorios");
                Console.WriteLine("2-Guardar " + numero + " numeros aleatorios");
                Console.WriteLine("3-Multiplicar por un número aleatorio");
                Console.WriteLine("4-Tabla de multiplicar hasta un numero aleatorio");
                Console.WriteLine("5-Salir");
                captura = Console.ReadLine();
                int opcion = int.Parse(captura);
                if (opcion == 1)
                {
                    LibreriaVisual.Libreria.numeros(numero);
                }
                if (opcion == 2)
                {
                    for (int i = 0; i < numero; i++)
                    {
                        Console.WriteLine(LibreriaVisual.Libreria.guardar(numero)[i]);
            
    }
                }
                if (opcion == 3)
                {
                    Console.WriteLine(LibreriaVisual.Libreria.multiplicar(numero));
                }
                if (opcion == 4)
                {

                  int[] tabla = LibreriaVisual.Libreria.tabla(numero);
                    for(int i = 0; i < tabla.Length; i++)
                    {
                        Console.WriteLine(tabla[i]);
                    }

                    
                }
                if (opcion == 5)
                {
                    repetir = false;
                }
            }
        }
    }
}
