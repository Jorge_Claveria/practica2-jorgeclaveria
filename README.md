Practica 2 de Entornos de Desarrollo de Jorge Claver�a

Eclipse:
	
	-Ventanas: Proyecto de ventana realizado con eclipse. 
	La ventana corresponde con la opci�n chat de la aplicaci�n.
	
	-Practica02: Proyecto de demostraci�n del uso de librerias en
	 eclipse con ejercicios num�ricos sencillos incluyendo bucles y arrays.

VisualStudio:

	-VentanaVisual: Proyecto de ventana realizado con VisualStudio.
	La ventana corresponde con una vista de mapa en la aplicaci�n.

	-ConsolaVisual: Proyecto de demostraci�n del uso de librerias en
	VisualStudio con ejercicios num�ricos sencillos incluyendo bucles y arrays.
