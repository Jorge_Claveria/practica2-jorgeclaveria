import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JMenuBar;
import javax.swing.SwingConstants;
import javax.swing.JTextPane;
import javax.swing.JScrollBar;
import java.awt.event.MouseWheelListener;
import java.awt.event.MouseWheelEvent;
import java.awt.Scrollbar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTabbedPane;
import javax.swing.JRadioButton;
import java.awt.Color;
import javax.swing.JSpinner;
import javax.swing.JLabel;
import javax.swing.SpinnerDateModel;
import java.util.Date;
import java.util.Calendar;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JSlider;
import javax.swing.JToolBar;

public class Principal extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal frame = new Principal();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Principal() {
		setResizable(false);
		setTitle("Jorge Claveria");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 320);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnMapa = new JMenu("Mapa");
		menuBar.add(mnMapa);
		
		JMenuItem mntmEditarMapa = new JMenuItem("Editar mapa");
		mnMapa.add(mntmEditarMapa);
		
		JMenuItem mntmVerMapa = new JMenuItem("Ver mapa");
		mnMapa.add(mntmVerMapa);
		
		JMenu mnAnimales = new JMenu("Animales");
		menuBar.add(mnAnimales);
		
		JMenuItem mntmVerAnimales = new JMenuItem("Ver animales");
		mnAnimales.add(mntmVerAnimales);
		
		JMenuItem mntmEditarAnimales = new JMenuItem("Editar animales");
		mnAnimales.add(mntmEditarAnimales);
		
		JMenu mnAyuda = new JMenu("Ayuda");
		menuBar.add(mnAyuda);
		
		JMenuItem mntmVerAyuda = new JMenuItem("Ver ayuda");
		mnAyuda.add(mntmVerAyuda);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(10, 41, 414, 209);
		contentPane.add(tabbedPane);
		
		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Juan", null, panel_1, null);
		panel_1.setLayout(null);
		
		JButton btnEnviar_1 = new JButton("Enviar");
		btnEnviar_1.setForeground(new Color(0, 128, 0));
		btnEnviar_1.setBounds(6, 147, 89, 23);
		panel_1.add(btnEnviar_1);
		
		textField_1 = new JTextField();
		textField_1.setBounds(117, 148, 282, 20);
		panel_1.add(textField_1);
		textField_1.setColumns(10);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(117, 11, 282, 128);
		panel_1.add(scrollPane_1);
		
		JTextArea txtrMensajeMensaje = new JTextArea();
		txtrMensajeMensaje.setEditable(false);
		txtrMensajeMensaje.setText("  Mensaje 1\r\n\r\n\t\t      Mensaje 2\r\n\r\n  Mensaje 3\r\n\r\n  \t\t      Mensaje 4\r\n\r\n  Mensaje 5  \r\n\r\n\t\t      Mensaje 6");
		scrollPane_1.setViewportView(txtrMensajeMensaje);
		
		JSpinner spinner = new JSpinner();
		spinner.setModel(new SpinnerDateModel(new Date(1550012400000L), new Date(1520895600000L), new Date(1550012400000L), Calendar.DAY_OF_YEAR));
		spinner.setBounds(6, 64, 70, 20);
		panel_1.add(spinner);
		
		JLabel lblFecha = new JLabel("Historial:");
		lblFecha.setBounds(6, 45, 70, 14);
		panel_1.add(lblFecha);
		
		JCheckBox chckbxSilenciar = new JCheckBox("Silenciar");
		chckbxSilenciar.setBounds(6, 91, 97, 23);
		panel_1.add(chckbxSilenciar);
		
		JCheckBox chckbxBloquear = new JCheckBox("Bloquear");
		chckbxBloquear.setForeground(new Color(255, 0, 0));
		chckbxBloquear.setBounds(6, 117, 97, 23);
		panel_1.add(chckbxBloquear);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setToolTipText("Reportar");
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Reportar", "Spam", "Acoso", "Contenido inapropiado"}));
		comboBox.setBounds(6, 14, 101, 20);
		panel_1.add(comboBox);
		
		JPanel panel_2 = new JPanel();
		tabbedPane.addTab("Ana", null, panel_2, null);
		panel_2.setLayout(null);
		
		JButton btnEnviar_2 = new JButton("Enviar");
		btnEnviar_2.setForeground(new Color(0, 128, 0));
		btnEnviar_2.setBounds(6, 147, 89, 23);
		panel_2.add(btnEnviar_2);
		
		textField_2 = new JTextField();
		textField_2.setBounds(117, 150, 282, 20);
		panel_2.add(textField_2);
		textField_2.setColumns(10);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(117, 11, 282, 128);
		panel_2.add(scrollPane);
		
		JTextArea txtrMensajeMensaje_1 = new JTextArea();
		txtrMensajeMensaje_1.setEditable(false);
		txtrMensajeMensaje_1.setText("  Mensaje 1\r\n\r\n\t\t      Mensaje 2\r\n\r\n  Mensaje 3\r\n\r\n  \t\t      Mensaje 4\r\n\r\n  Mensaje 5  \r\n\r\n\t\t      Mensaje 6");
		scrollPane.setViewportView(txtrMensajeMensaje_1);
		
		JCheckBox chckbxSilenciar_1 = new JCheckBox("Silenciar");
		chckbxSilenciar_1.setBounds(6, 80, 97, 23);
		panel_2.add(chckbxSilenciar_1);
		
		JCheckBox chckbxBloquear_1 = new JCheckBox("Bloquear");
		chckbxBloquear_1.setForeground(new Color(255, 0, 0));
		chckbxBloquear_1.setBounds(6, 106, 97, 23);
		panel_2.add(chckbxBloquear_1);
		
		JSpinner spinner_1 = new JSpinner();
		spinner_1.setModel(new SpinnerDateModel(new Date(1550012400000L), new Date(1520895600000L), new Date(1550012400000L), Calendar.DAY_OF_YEAR));
		spinner_1.setBounds(6, 53, 70, 20);
		panel_2.add(spinner_1);
		
		JLabel lblHistorial = new JLabel("Historial:");
		lblHistorial.setBounds(6, 32, 46, 14);
		panel_2.add(lblHistorial);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setModel(new DefaultComboBoxModel(new String[] {"Reportar", "Spam", "Acoso", "Contenido inapropiado"}));
		comboBox_1.setBounds(6, 11, 101, 20);
		panel_2.add(comboBox_1);
		
		JPanel panel = new JPanel();
		panel.setForeground(new Color(0, 0, 0));
		tabbedPane.addTab("Mario", null, panel, null);
		panel.setLayout(null);
		
		JButton btnEnviar = new JButton("Enviar");
		btnEnviar.setForeground(new Color(0, 128, 0));
		btnEnviar.setBounds(6, 147, 89, 23);
		panel.add(btnEnviar);
		
		textField = new JTextField();
		textField.setBounds(117, 148, 282, 20);
		panel.add(textField);
		textField.setColumns(10);
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(117, 11, 282, 128);
		panel.add(scrollPane_2);
		
		JTextArea txtrMensaje = new JTextArea();
		txtrMensaje.setEditable(false);
		txtrMensaje.setText("  Mensaje 1\r\n\r\n\t\t      Mensaje 2\r\n\r\n  Mensaje 3\r\n\r\n  \t\t      Mensaje 4\r\n\r\n  Mensaje 5  \r\n\r\n\t\t      Mensaje 6");
		scrollPane_2.setViewportView(txtrMensaje);
		
		JCheckBox chckbxSilenciar_2 = new JCheckBox("Silenciar");
		chckbxSilenciar_2.setBounds(6, 80, 97, 23);
		panel.add(chckbxSilenciar_2);
		
		JCheckBox chckbxBloquear_2 = new JCheckBox("Bloquear");
		chckbxBloquear_2.setForeground(new Color(255, 0, 0));
		chckbxBloquear_2.setBounds(6, 106, 97, 23);
		panel.add(chckbxBloquear_2);
		
		JSpinner spinner_2 = new JSpinner();
		spinner_2.setModel(new SpinnerDateModel(new Date(1550012400000L), new Date(1520895600000L), new Date(1550012400000L), Calendar.DAY_OF_YEAR));
		spinner_2.setBounds(10, 53, 70, 20);
		panel.add(spinner_2);
		
		JLabel lblHistorial_1 = new JLabel("Historial:");
		lblHistorial_1.setBounds(10, 31, 46, 14);
		panel.add(lblHistorial_1);
		
		JComboBox comboBox_2 = new JComboBox();
		comboBox_2.setModel(new DefaultComboBoxModel(new String[] {"Reportar", "Spam", "Acoso", "Contenido inapropiado"}));
		comboBox_2.setBounds(6, 11, 101, 20);
		panel.add(comboBox_2);
		
		JSlider slider = new JSlider();
		slider.setBounds(209, 20, 168, 26);
		contentPane.add(slider);
		
		JLabel lblVolumen = new JLabel("Volumen:");
		lblVolumen.setBounds(146, 32, 68, 14);
		contentPane.add(lblVolumen);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setBounds(0, 0, 329, 23);
		contentPane.add(toolBar);
		
		JButton btnAgregarChar = new JButton("Agregar chat");
		toolBar.add(btnAgregarChar);
		
		JButton btnEditarFuente = new JButton("Editar fuente");
		toolBar.add(btnEditarFuente);
		
		JButton btnCambiarColor = new JButton("Cambiar color");
		toolBar.add(btnCambiarColor);
	}
}
